CREATE LOGIN GOP_DEVELOPER WITH PASSWORD = 'gop', CHECK_POLICY = OFF;


USE [master]
GO
ALTER SERVER ROLE [dbcreator] ADD MEMBER [GOP_DEVELOPER]
GO
ALTER SERVER ROLE [diskadmin] ADD MEMBER [GOP_DEVELOPER]
GO
ALTER SERVER ROLE [processadmin] ADD MEMBER [GOP_DEVELOPER]
GO
ALTER SERVER ROLE [securityadmin] ADD MEMBER [GOP_DEVELOPER]
GO
ALTER SERVER ROLE [serveradmin] ADD MEMBER [GOP_DEVELOPER]
GO
ALTER SERVER ROLE [setupadmin] ADD MEMBER [GOP_DEVELOPER]
GO
ALTER SERVER ROLE [sysadmin] ADD MEMBER [GOP_DEVELOPER]
GO

CREATE DATABASE GOP_BEN;
GO
USE [GOP_BEN]
GO
CREATE USER [GOP_DEVELOPER] FOR LOGIN [GOP_DEVELOPER]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [GOP_DEVELOPER]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [GOP_DEVELOPER]
GO
ALTER ROLE [db_datareader] ADD MEMBER [GOP_DEVELOPER]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [GOP_DEVELOPER]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [GOP_DEVELOPER]
GO
ALTER ROLE [db_denydatareader] ADD MEMBER [GOP_DEVELOPER]
GO
ALTER ROLE [db_denydatawriter] ADD MEMBER [GOP_DEVELOPER]
GO
ALTER ROLE [db_owner] ADD MEMBER [GOP_DEVELOPER]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [GOP_DEVELOPER]
GO



CREATE DATABASE mpp_gop_ben;
go

USE mpp_gop_ben
Go

CREATE USER GOP_DEVELOPER FOR LOGIN GOP_DEVELOPER;
GO

ALTER ROLE [db_accessadmin] ADD MEMBER [GOP_DEVELOPER]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [GOP_DEVELOPER]
GO
ALTER ROLE [db_datareader] ADD MEMBER [GOP_DEVELOPER]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [GOP_DEVELOPER]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [GOP_DEVELOPER]
GO
ALTER ROLE [db_denydatareader] ADD MEMBER [GOP_DEVELOPER]
GO
ALTER ROLE [db_denydatawriter] ADD MEMBER [GOP_DEVELOPER]
GO
ALTER ROLE [db_owner] ADD MEMBER [GOP_DEVELOPER]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [GOP_DEVELOPER]
GO 

 