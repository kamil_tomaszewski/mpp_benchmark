/*
#Full code: benchmark.loooop

# DO NOT REMOVE, MOVE OR MODIFY THIS COMMENT
# unless you know what you're doing. Incorrect content will cause error during import.
# IMPORTANT: these escape sequences MUST be used inside this comment: '/' -> '\/', '\' -> '\\'

categories = []

[[arguments]]
name = "ctx"
type = "SPECIFIED_CLASS"
className = "org.smartparam.engine.core.context.ParamContext"
*/

def groups = ctx.get('offer.groups')
BigDecimal result = BigDecimal.ZERO
for (def group : groups) {
  for (def scope : group.get('scopes')) {
     for (def variant : scope.get('variants')) {
        for (def rider : variant.get('riders')) {
           for (def insured : group.get('insureds')) {
              result = calc(result, insured, rider, variant.get('name'), scope.get('name'), group.get('name'))
            }
        }
     }
  }
}
