CREATE TABLE [dbo].[INSURED]
(
    [id]         [bigint]        NOT NULL,
    [bundle_id]  [bigint]        NOT NULL,
    [parent_id]  [bigint]        NOT NULL,
    [owner_prop] [nvarchar](100) NULL,

    [birth_year] [nvarchar](4)   NULL,
    [holder_nip] [nvarchar](10)  NULL,
    [salary]     decimal         NULL,
    [sex]        [nvarchar](10)  NULL,

    CONSTRAINT [PK_INSURED] PRIMARY KEY CLUSTERED
        (
         [id] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [INSURED_BUNDLE_ID_IDX] ON [dbo].[INSURED]
    (
     [bundle_id] ASC
        ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [INSURED_PARENT_ID_IDX] ON [dbo].[INSURED]
    (
     [parent_id] ASC
        ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [INSURED_OWNER_PROP_IDX] ON [dbo].[INSURED]
    (
     [owner_prop] ASC
        ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

CREATE TABLE [dbo].[INSURED_H]
(
    [id]         [bigint]       NOT NULL,
    [parent_id]  [bigint]       NOT NULL,
    [birth_year] [nvarchar](4)  NULL,
    [holder_nip] [nvarchar](10) NULL,
    [salary]     decimal        NULL,
    [sex]        [nvarchar](10) NULL,
    CONSTRAINT [PK_INSURED_H] PRIMARY KEY CLUSTERED
        (
         [id] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE NONCLUSTERED INDEX [INSURED_H_parent_id_IDX] ON [dbo].[INSURED_H]
    (
     [parent_id] ASC
        ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


CREATE SEQUENCE [dbo].[INSURED_H_SEQ]
    AS [bigint]
    START WITH 1
    INCREMENT BY 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE
GO
