IF EXISTS(SELECT *
          FROM sys.objects
          WHERE object_id = OBJECT_ID(N'[dbo].[BUNDLE_SEQ]')
            AND type = 'SO')
    DROP SEQUENCE [dbo].[BUNDLE_SEQ]

CREATE SEQUENCE [dbo].[BUNDLE_SEQ]
    AS [bigint]
    START WITH 10
    INCREMENT BY 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE
GO

CREATE TABLE [dbo].[BUNDLE]
(
    [id]       [bigint]        NOT NULL,
    [revision] [bigint]        NULL,
    [created]  [datetime]      NULL,
    [updated]  [datetime]      NULL,
    [value]    [nvarchar](max) NULL,
    CONSTRAINT [PK_BUNDLE] PRIMARY KEY CLUSTERED
        (
         [id] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[BUNDLE_H]
(
    [id]   [bigint]      NOT NULL,
    [uuid] [varchar](64) NULL,
    CONSTRAINT [PK_BUNDLE_H] PRIMARY KEY CLUSTERED
        (
         [id] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE SEQUENCE [dbo].[BUNDLE_H_SEQ]
    AS [bigint]
    START WITH 10
    INCREMENT BY 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE
GO

CREATE SEQUENCE [dbo].[app_seq]
    AS [bigint]
    START WITH 10
    INCREMENT BY 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE
GO