CREATE TABLE [dbo].[RIDER]
(
    [id]          [bigint]        NOT NULL,
    [bundle_id]   [bigint]        NOT NULL,
    [parent_id]   [bigint]        NOT NULL,
    [owner_prop]  [nvarchar](100) NULL,

    [assured_sum] decimal         NULL,
    [orderr]      [bigint]        NULL,
    [code]        [nvarchar](32)  NULL,
    [uid]         [nvarchar](64)  NULL,

    CONSTRAINT [PK_RIDER] PRIMARY KEY CLUSTERED
        (
         [id] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [RIDER_BUNDLE_ID_IDX] ON [dbo].[RIDER]
    (
     [bundle_id] ASC
        ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [RIDER_PARENT_ID_IDX] ON [dbo].[RIDER]
    (
     [parent_id] ASC
        ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [RIDER_OWNER_PROP_IDX] ON [dbo].[RIDER]
    (
     [owner_prop] ASC
        ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


CREATE TABLE [dbo].[RIDER_H]
(
    [id]          [bigint]       NOT NULL,
    [uuid]        [varchar](64)  NULL,
    [parent_id]   [bigint]       NOT NULL,
    [assured_sum] decimal        NULL,
    [code]        [nvarchar](32) NULL,
    [order]       [bigint]       NULL,
    CONSTRAINT [PK_RIDER_H] PRIMARY KEY CLUSTERED
        (
         [id] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE NONCLUSTERED INDEX [RIDER_H_parent_id_IDX] ON [dbo].[RIDER_H]
    (
     [parent_id] ASC
        ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


CREATE SEQUENCE [dbo].[RIDER_H_SEQ]
    AS [bigint]
    START WITH 10
    INCREMENT BY 1
    MINVALUE 0
    MAXVALUE 9223372036854775807
    CACHE
GO