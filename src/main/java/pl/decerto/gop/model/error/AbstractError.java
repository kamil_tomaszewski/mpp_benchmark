package pl.decerto.gop.model.error;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public abstract class AbstractError {

	/**
	 * Violation Subject
	 */
	private Object rejectedValue;

	/**
	 * Violation code for frontend
	 */
	private String errorCode;
}
