package pl.decerto.gop.model.bundle;

import lombok.Value;

import java.io.Serializable;

@Value
public class BundleId implements Serializable {

	Long value;
}
