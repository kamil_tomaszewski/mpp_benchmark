package pl.decerto.gop.model.rider;

import lombok.NonNull;
import lombok.Value;

/**
 * @author Krzysztof Wilczak <krzysztof.wilczak@nn.pl>
 * @since 2018-06-29
 */
@Value
public class RiderCode {

	public static final RiderCode MAIN = new RiderCode("MAIN");

	@NonNull
	String value;
}
