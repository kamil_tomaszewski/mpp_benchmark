package pl.decerto.gop.model.rider;

import lombok.Value;

import java.io.Serializable;

@Value
public class RiderId implements Serializable {

	Long value;
}
