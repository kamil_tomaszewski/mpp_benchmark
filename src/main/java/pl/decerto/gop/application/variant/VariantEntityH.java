package pl.decerto.gop.application.variant;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import pl.decerto.gop.application.rider.RiderEntityH;
import pl.decerto.gop.application.scope.ScopeEntityH;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "VARIANT_H")
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class VariantEntityH {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "VARIANT_H_GENERATOR")
	@SequenceGenerator(name = "VARIANT_H_GENERATOR", sequenceName = "VARIANT_H_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	private String uuid;
	private String name;

	@OneToMany(mappedBy = "parent", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<RiderEntityH> riders = new ArrayList<>();

	@ManyToOne(fetch = FetchType.LAZY)
	private ScopeEntityH parent;

	public void setParent(ScopeEntityH parent) {

		this.parent = parent;
		parent.getVariants().add(this);
	}

}
