package pl.decerto.gop.application.variant;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VariantEntityHRepo extends JpaRepository<VariantEntityH, Long> {

}
