package pl.decerto.gop.application.test;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.decerto.gop.application.bundle.BundleEntityH;
import pl.decerto.gop.application.group.GroupService;
import pl.decerto.gop.application.offer.OfferService;
import pl.decerto.gop.infrastructure.util.BundleRootUtils;

@RestController
@AllArgsConstructor
public class Controller {

	private final OfferService offerService;
	private final GroupService groupsService;
	private final BundleRootUtils bundleRootUtils;

	BundleEntityH createBundleH() {

		return offerService.createEntity();
	}

	@GetMapping("two-group-h")
	public Long twoGroupH() {

		BundleEntityH bundle = createBundleH();
		groupsService.addEntity(bundle.getOffer(), "second");
		return bundleRootUtils.saveLoadAndCheckRiders(bundle, 2 * 50, 0).getId();
	}

}
