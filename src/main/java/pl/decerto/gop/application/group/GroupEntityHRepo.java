package pl.decerto.gop.application.group;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupEntityHRepo extends JpaRepository<GroupEntityH, Long> {

}
