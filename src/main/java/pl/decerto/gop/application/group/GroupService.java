package pl.decerto.gop.application.group;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.decerto.gop.application.offer.OfferEntityH;
import pl.decerto.gop.application.scope.ScopeService;
import pl.decerto.gop.hyperon.persistence.entities.Group;
import pl.decerto.gop.hyperon.persistence.entities.Offer;

@Service
@Transactional
@AllArgsConstructor
public class GroupService {

	private final ScopeService scopeService;

	public Group add(Offer offer, String name) {

		Group group = new Group();
		group.setName(name);
		scopeService.add(group, "Scope 1");
		offer.getGroups().add(group);
		return group;
	}

	public GroupEntityH addEntity(OfferEntityH offer, String name) {

		GroupEntityH group = new GroupEntityH();
		group.setName(name);
		scopeService.addEntity(group, "Scope 1");
		group.setParent(offer);
		return group;
	}

}
