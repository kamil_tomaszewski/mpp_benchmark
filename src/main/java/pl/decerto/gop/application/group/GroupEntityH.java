package pl.decerto.gop.application.group;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import pl.decerto.gop.application.insured.InsuredEntityH;
import pl.decerto.gop.application.offer.OfferEntityH;
import pl.decerto.gop.application.scope.ScopeEntityH;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "GROUP_H")
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class GroupEntityH {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GROUP_H_GENERATOR")
	@SequenceGenerator(name = "GROUP_H_GENERATOR", sequenceName = "GROUP_H_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	private String uuid;
	private String name;

	@OneToMany(mappedBy = "parent", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<ScopeEntityH> scopes = new ArrayList<>();

	@OneToMany(mappedBy = "parent", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<InsuredEntityH> insureds = new ArrayList<>();

	@ManyToOne(fetch = FetchType.LAZY)
	private OfferEntityH parent;

	public void setParent(OfferEntityH parent) {

		this.parent = parent;
		parent.getGroups().add(this);
	}

}
