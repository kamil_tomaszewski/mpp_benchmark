package pl.decerto.gop.application.insured;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.decerto.gop.application.scope.ScopeEntityH;

@Repository
public interface InsuredEntityHRepo extends JpaRepository<ScopeEntityH, Long> {

}
