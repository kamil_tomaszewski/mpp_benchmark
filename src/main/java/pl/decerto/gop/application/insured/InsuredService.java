package pl.decerto.gop.application.insured;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.decerto.gop.application.bundle.BundleEntityH;
import pl.decerto.gop.application.bundle.BundleEntityHRepo;
import pl.decerto.gop.application.bundle.DomainBundleService;
import pl.decerto.gop.application.group.GroupEntityH;
import pl.decerto.gop.hyperon.persistence.entities.BundleRoot;
import pl.decerto.gop.hyperon.persistence.entities.Group;
import pl.decerto.gop.hyperon.persistence.entities.Insured;
import pl.decerto.hyperon.persistence.sync.diff.BundleDiff;

import java.math.BigDecimal;
import java.util.Random;

@Slf4j
@Service
@Transactional
@AllArgsConstructor
public class InsuredService {

	private final static Random generator = new Random();
	private final BundleEntityHRepo bundleRepoH;
	private final DomainBundleService bundleService;

	public Insured add(Group group, String name) {

		Insured insured = new Insured();
		insured.setBirthYear("1992");
		insured.setSex("F");
		insured.setHolderNip(name);
		insured.setSalary(BigDecimal.valueOf(generator.nextDouble()));
		group.getInsureds().add(insured);
		return insured;
	}

	public InsuredEntityH addEntity(GroupEntityH group, String name) {

		InsuredEntityH insured = new InsuredEntityH();
		insured.setBirthYear("1992");
		insured.setSex("F");
		insured.setHolderNip(name);
		insured.setSalary(BigDecimal.valueOf(generator.nextDouble()));
		insured.setParent(group);
		return insured;
	}

	public BundleDiff addInsuredLoadSave(BundleRoot bundle) {

		bundle = bundleService.load(bundle.getId());
		add(bundle.getOffer().getGroups().get(0), "new");
		return bundleService.persist(bundle);
	}

	public BundleEntityH addInsuredHLoadSave(BundleEntityH bundle) {

		bundle = bundleRepoH.findById(bundle.getId()).get();
		addEntity(bundle.getOffer().getGroups().get(0), "new");
		return bundleRepoH.saveAndFlush(bundle);
	}

}
