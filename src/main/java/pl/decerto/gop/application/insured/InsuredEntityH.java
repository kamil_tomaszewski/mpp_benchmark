package pl.decerto.gop.application.insured;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import pl.decerto.gop.application.group.GroupEntityH;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "INSURED_H")
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class InsuredEntityH {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "INSURED_H_GENERATOR")
	@SequenceGenerator(name = "INSURED_H_GENERATOR", sequenceName = "INSURED_H_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;

	private String birthYear;
	private String holderNip;
	private BigDecimal salary;
	private String sex;

	@ManyToOne(fetch = FetchType.LAZY)
	private GroupEntityH parent;

	public void setParent(GroupEntityH parent) {

		this.parent = parent;
		parent.getInsureds().add(this);
	}
}
