package pl.decerto.gop.application.bundle;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import pl.decerto.gop.application.offer.OfferEntityH;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "BUNDLE_H")
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class BundleEntityH {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BUNDLE_H_GENERATOR")
	@SequenceGenerator(name = "BUNDLE_H_GENERATOR", sequenceName = "BUNDLE_H_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	private String uuid;

	@OneToOne(mappedBy = "parent", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private OfferEntityH offer;
}
