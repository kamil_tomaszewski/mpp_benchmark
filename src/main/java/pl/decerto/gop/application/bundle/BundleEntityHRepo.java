package pl.decerto.gop.application.bundle;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BundleEntityHRepo extends JpaRepository<BundleEntityH, Long> {

}
