package pl.decerto.gop.application.bundle;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.decerto.gop.hyperon.persistence.entities.BundleRoot;
import pl.decerto.gop.hyperon.service.persistence.PersistenceService;
import pl.decerto.gop.infrastructure.aspects.monitor.PerformanceLogging;
import pl.decerto.gop.infrastructure.exception.application.BundleNotFoundException;
import pl.decerto.gop.model.bundle.BundleId;
import pl.decerto.hyperon.persistence.sync.diff.BundleDiff;

@Slf4j
@Service
@Transactional
@AllArgsConstructor
public class DomainBundleService {

	private final PersistenceService persistenceService;

	public BundleRoot create() {

		return persistenceService.create();
	}

	@PerformanceLogging
	public BundleDiff persist(BundleRoot root) {

		return persistenceService.persist(root);
	}

	@PerformanceLogging
	public BundleRoot load(long id) {

		return load(new BundleId(id));
	}

	@PerformanceLogging
	public BundleRoot load(BundleId bundleId) {

		try {
			return persistenceService.load(bundleId);
		} catch (EmptyResultDataAccessException exception) {
			log.error(exception.getMessage(), exception);
			throw new BundleNotFoundException(bundleId);
		}
	}
}
