package pl.decerto.gop.application.offer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OfferEntityHRepo extends JpaRepository<OfferEntityH, Long> {

}
