package pl.decerto.gop.application.offer;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.decerto.gop.application.bundle.BundleEntityH;
import pl.decerto.gop.application.bundle.BundleEntityHRepo;
import pl.decerto.gop.application.bundle.DomainBundleService;
import pl.decerto.gop.application.group.GroupService;
import pl.decerto.gop.application.insured.InsuredService;
import pl.decerto.gop.application.scope.ScopeService;
import pl.decerto.gop.application.variant.VariantService;
import pl.decerto.gop.hyperon.persistence.entities.BundleRoot;
import pl.decerto.gop.infrastructure.util.BundleRootUtils;

@Slf4j
@Service
@Transactional
@AllArgsConstructor
public class OfferService {

	private final GroupService groupService;
	private final DomainBundleService bundleService;
	private final BundleEntityHRepo bundleEntityHRepo;
	private final BundleRootUtils bundleRootUtils;
	private final ScopeService scopeService;
	private final VariantService variantService;
	private final InsuredService insuredService;

	public BundleRoot create() {

		BundleRoot bundleRoot = bundleService.create();
		groupService.add(bundleRoot.getOffer(), "first");
		return bundleRoot;
	}

	public BundleRoot createBundleWith(int bundles, int scopes, int variants, int members) {

		BundleRoot bundle = create();
		addGroupsToAll(bundle, bundles - 1);
		addScopesToAll(bundle, scopes - 1);
		addVariantsToAll(bundle, variants - 1);
		addInsuredsToAll(bundle, members);
		return bundle;
	}

	BundleRoot addGroupsToAll(BundleRoot bundle, int size) {

		for (int i = 0; i < size; i++) {
			groupService.add(bundle.getOffer(), "groupName" + i + 2);
		}
		return bundle;
	}

	BundleRoot addScopesToAll(BundleRoot bundle, int size) {

		bundle.getOffer().getGroups().forEach(g -> {
			for (int i = 0; i < size; i++) {
				scopeService.add(g, "scope" + i + 2);
			}
		});
		return bundle;
	}

	BundleRoot addVariantsToAll(BundleRoot bundle, int size) {

		bundleRootUtils.getScopesStream(bundle).forEach(s -> {
			for (int i = 0; i < size; i++) {
				variantService.add(s, "scope" + i + 2);
			}
		});
		return bundle;
	}

	BundleRoot addInsuredsToAll(BundleRoot bundle, int size) {

		bundle.getOffer().getGroups().forEach(g -> {
			for (int i = 0; i < size; i++) {
				insuredService.add(g, "i" + i + 2);
			}
		});
		return bundle;
	}

	public BundleEntityH createEntity() {

		BundleEntityH bundle = new BundleEntityH();
		OfferEntityH offer = new OfferEntityH();
		offer.setParent(bundle);
		groupService.addEntity(bundle.getOffer(), "first");
		bundleEntityHRepo.save(bundle);
		return bundle;
	}

	public BundleEntityH createBundleHWith(int bundles, int scopes, int variants, int members) {

		BundleEntityH bundle = createEntity();
		addGroupsToAll(bundle, bundles - 1);
		addScopesToAll(bundle, scopes - 1);
		addVariantsToAll(bundle, variants - 1);
		addInsuredsToAll(bundle, members);
		return bundle;
	}

	BundleEntityH addGroupsToAll(BundleEntityH bundle, int size) {

		for (int i = 0; i < size; i++) {
			groupService.addEntity(bundle.getOffer(), "groupName" + i + 2);
		}
		return bundle;
	}

	BundleEntityH addScopesToAll(BundleEntityH bundle, int size) {

		bundle.getOffer().getGroups().forEach(g -> {
			for (int i = 0; i < size; i++) {
				scopeService.addEntity(g, "scope" + i + 2);
			}
		});
		return bundle;
	}

	BundleEntityH addVariantsToAll(BundleEntityH bundle, int size) {

		bundleRootUtils.getScopesStream(bundle).forEach(s -> {
			for (int i = 0; i < size; i++) {
				variantService.addEntity(s, "scope" + i + 2);
			}
		});
		return bundle;
	}

	BundleEntityH addInsuredsToAll(BundleEntityH bundle, int size) {

		bundle.getOffer().getGroups().forEach(g -> {
			for (int i = 0; i < size; i++) {
				insuredService.addEntity(g, "i" + i + 2);
			}
		});
		return bundle;
	}

}
