package pl.decerto.gop.application.offer;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import pl.decerto.gop.application.bundle.BundleEntityH;
import pl.decerto.gop.application.group.GroupEntityH;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "OFFER_H")
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class OfferEntityH {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OFFER_H_GENERATOR")
	@SequenceGenerator(name = "OFFER_H_GENERATOR", sequenceName = "OFFER_H_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	private String uuid;

	@OneToMany(mappedBy = "parent", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<GroupEntityH> groups = new ArrayList<>();

	@OneToOne(fetch = FetchType.LAZY)
	private BundleEntityH parent;

	public void setParent(BundleEntityH parent) {

		this.parent = parent;
		parent.setOffer(this);
	}
}
