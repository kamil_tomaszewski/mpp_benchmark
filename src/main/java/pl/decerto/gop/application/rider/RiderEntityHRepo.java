package pl.decerto.gop.application.rider;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RiderEntityHRepo extends JpaRepository<RiderEntityH, Long> {

}
