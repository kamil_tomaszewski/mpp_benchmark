package pl.decerto.gop.application.rider;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import pl.decerto.gop.application.variant.VariantEntityH;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "RIDER_H")
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class RiderEntityH {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RIDER_H_GENERATOR")
	@SequenceGenerator(name = "RIDER_H_GENERATOR", sequenceName = "RIDER_H_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	private BigDecimal assuredSum;
	private String code;
	private String uuid;

	@ManyToOne(fetch = FetchType.LAZY)
	private VariantEntityH parent;

	public void setParent(VariantEntityH parent) {

		this.parent = parent;
		parent.getRiders().add(this);
	}

}
