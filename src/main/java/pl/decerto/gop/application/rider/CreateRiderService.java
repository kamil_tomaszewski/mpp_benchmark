package pl.decerto.gop.application.rider;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.decerto.gop.application.scope.ScopeEntityH;
import pl.decerto.gop.hyperon.persistence.entities.Rider;
import pl.decerto.gop.hyperon.persistence.entities.Scope;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@AllArgsConstructor
public class CreateRiderService {

	private final static Random generator = new Random();

	public void prepareScopeRiders(Scope scope) {

		scope.getVariants().forEach(variant ->
				variant.getRiders().addAll(createInitialRiders())
		);
	}

	public List<Rider> createInitialRiders() {

		return IntStream.range(0, 50).boxed().
				map(c -> createRider(Integer.toString(c)))
				.collect(Collectors.toList());
	}

	private Rider createRider(String code) {

		Rider rider = new Rider();
		rider.setCode(code);
		rider.setAssuredSum(BigDecimal.valueOf(generator.nextDouble()));
		return rider;
	}

	void prepareScopeRidersEntity(ScopeEntityH scope) {

		scope.getVariants().forEach(t -> createInitialRidersEntity().forEach(r -> r.setParent(t)));
	}

	public List<RiderEntityH> createInitialRidersEntity() {

		return IntStream.range(0, 50).boxed().
				map(c -> createRiderEntity(Integer.toString(c)))
				.collect(Collectors.toList());
	}

	private RiderEntityH createRiderEntity(String code) {

		RiderEntityH rider = new RiderEntityH();
		rider.setCode(code);
		rider.setAssuredSum(BigDecimal.valueOf(generator.nextDouble()));
		return rider;
	}
}
