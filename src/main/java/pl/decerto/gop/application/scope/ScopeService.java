package pl.decerto.gop.application.scope;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.decerto.gop.application.group.GroupEntityH;
import pl.decerto.gop.application.variant.VariantService;
import pl.decerto.gop.hyperon.persistence.entities.Group;
import pl.decerto.gop.hyperon.persistence.entities.Scope;

@Slf4j
@Service
@Transactional
@AllArgsConstructor
public class ScopeService {

	private final VariantService variantService;

	public Scope add(Group group, String name) {

		Scope scope = new Scope();
		scope.setName(name);
		variantService.add(scope, "Variant 1");
		group.getScopes().add(scope);
		return scope;
	}

	public ScopeEntityH addEntity(GroupEntityH group, String name) {

		ScopeEntityH scope = new ScopeEntityH();
		scope.setName(name);
		variantService.addEntity(scope, "Variant 1");
		scope.setParent(group);
		return scope;
	}

}
