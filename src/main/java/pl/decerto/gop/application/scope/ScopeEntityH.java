package pl.decerto.gop.application.scope;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import pl.decerto.gop.application.group.GroupEntityH;
import pl.decerto.gop.application.variant.VariantEntityH;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SCOPE_H")
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class ScopeEntityH {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SCOPE_H_GENERATOR")
	@SequenceGenerator(name = "SCOPE_H_GENERATOR", sequenceName = "SCOPE_H_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	private String uuid;
	private String name;

	@OneToMany(mappedBy = "parent", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<VariantEntityH> variants = new ArrayList<>();

	@ManyToOne(fetch = FetchType.LAZY)
	private GroupEntityH parent;

	public void setParent(GroupEntityH parent) {

		this.parent = parent;
		parent.getScopes().add(this);
	}

}
