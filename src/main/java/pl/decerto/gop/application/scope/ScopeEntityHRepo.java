package pl.decerto.gop.application.scope;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScopeEntityHRepo extends JpaRepository<ScopeEntityH, Long> {

}
