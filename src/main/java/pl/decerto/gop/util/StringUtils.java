package pl.decerto.gop.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class StringUtils {

	public static final String SPACE = " ";
	public static final String EMPTY = "";
}