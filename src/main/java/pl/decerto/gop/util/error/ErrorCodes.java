package pl.decerto.gop.util.error;

public interface ErrorCodes {

	interface Rider {

		String ERR_RIDER_NOT_FOUND = "ERR_RIDER_NOT_FOUND";
		String ERR_RIDER_ID_IS_REQUIRED = "ERR_RIDER_ID_IS_REQUIRED";
		String ERR_RIDER_ASSURED_SUM_IS_REQUIRED = "ERR_RIDER_ASSURED_SUM_IS_REQUIRED";
		String ERR_RIDER_ASSURED_SUM_IS_TO_SMALL = "ERR_RIDER_ASSURED_SUM_IS_TO_SMALL";
	}
}
