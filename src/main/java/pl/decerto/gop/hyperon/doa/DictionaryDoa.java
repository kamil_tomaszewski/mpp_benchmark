package pl.decerto.gop.hyperon.doa;

import pl.decerto.gop.hyperon.doa.schema.AbstractDoa;
import pl.decerto.hyperon.runtime.core.AdhocContext;
import pl.decerto.hyperon.runtime.model.HyperonDomainObject;

public class DictionaryDoa extends AbstractDoa<DictionaryDoa> {

	public DictionaryDoa(HyperonDomainObject domainObject) {

		super(domainObject);
	}

	public Boolean getConst(String formula) {

		AdhocContext context = new AdhocContext();
		context.set("formula", formula);
		return with(context).getBoolean(Attributes.Basic.CONST.name());
	}

	static class Attributes {

		enum Basic {

			CONST,
		}
	}
}
