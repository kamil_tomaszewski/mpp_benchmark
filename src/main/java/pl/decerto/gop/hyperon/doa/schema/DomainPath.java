package pl.decerto.gop.hyperon.doa.schema;

public enum DomainPath {

	DICTIONARY("/DICTIONARY");

	private final String path;

	DomainPath(String path) {

		this.path = path;
	}

	public String getPath(Object... args) {

		return String.format(path, args);
	}
}
