package pl.decerto.gop.hyperon.doa.schema;

import lombok.Getter;
import org.smartparam.engine.core.output.ParamValue;
import org.smartparam.engine.core.type.ValueHolder;
import pl.decerto.hyperon.runtime.core.AdhocContext;
import pl.decerto.hyperon.runtime.core.EmptyParamValue;
import pl.decerto.hyperon.runtime.core.HyperonContext;
import pl.decerto.hyperon.runtime.exception.HyperonIllegalStateException;
import pl.decerto.hyperon.runtime.helper.TypeConverter;
import pl.decerto.hyperon.runtime.model.HyperonDomainObject;

public abstract class AbstractDoa<T> {

	private static final EmptyParamValue EMPTY_PARAM_VALUE = new EmptyParamValue();
	private static final TypeConverter TYPE_CONVERTER = new TypeConverter();

	protected final HyperonDomainObject domain;
	@Getter
	private HyperonContext ctx;

	protected AbstractDoa(HyperonDomainObject domain) {

		if (domain == null) {
			throw new HyperonIllegalStateException("Domain object can't be null");
		}
		this.domain = domain;
		this.ctx = new AdhocContext();
	}

	@SuppressWarnings("unchecked")
	public T with(HyperonContext ctx) {

		this.ctx = ctx != null ? new AdhocContext(ctx) : new AdhocContext();
		return (T) this;
	}

	protected Object getUnwrappedValue(String code) {

		ValueHolder holder = getValue(code).getHolder();
		return holder != null ? holder.getValue() : null;
	}

	protected ParamValue getValue(String code) {

		ParamValue value = domain.getAttributeValue(null, code, ctx);
		return value != null ? value : EMPTY_PARAM_VALUE;
	}

	protected boolean getBoolean(String code) {

		return TYPE_CONVERTER.getBoolean(getUnwrappedValue(code));
	}
}
