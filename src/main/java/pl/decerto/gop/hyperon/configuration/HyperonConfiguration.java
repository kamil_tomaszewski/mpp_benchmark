package pl.decerto.gop.hyperon.configuration;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.decerto.gop.hyperon.configuration.properties.HyperonProperties;
import pl.decerto.hyperon.persistence.factory.HyperonPersistenceFactory;
import pl.decerto.hyperon.persistence.proxy.HyperonPersistence;
import pl.decerto.hyperon.persistence.service.BundleService;
import pl.decerto.hyperon.runtime.core.HyperonEngine;
import pl.decerto.hyperon.runtime.core.HyperonEngineFactory;
import pl.decerto.hyperon.runtime.sql.DialectRegistry;
import pl.decerto.hyperon.runtime.sql.DialectTemplate;

import javax.sql.DataSource;

@Slf4j
@Configuration
public class HyperonConfiguration {

	private final HyperonProperties hyperonProperties;
	private final DataSource primaryDataSource;

	public HyperonConfiguration(HyperonProperties hyperonProperties, DataSource primaryDataSource) {

		this.hyperonProperties = hyperonProperties;
		this.primaryDataSource = primaryDataSource;
	}

	@Bean
	public DataSource getDataSource() {

		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setUsername(hyperonProperties.getUsername());
		dataSource.setPassword(hyperonProperties.getPassword());
		dataSource.setUrl(hyperonProperties.getUrl());
		dataSource.setInitialSize(hyperonProperties.getRuntime().getInitialSize());
		dataSource.setMaxActive(hyperonProperties.getRuntime().getMaxActive());
		dataSource.setDriverClassName(getDialectTemplate().getJdbcDriverClassName());
		return dataSource;
	}

	@Bean
	public DialectRegistry getDialectRegistry() {

		DialectRegistry registry = new DialectRegistry();
		registry.setDialect(hyperonProperties.getDialect());
		return registry;
	}

	@Bean
	public DialectTemplate getDialectTemplate() {

		return getDialectRegistry().create();
	}

	@Bean(destroyMethod = "destroy")
	public HyperonEngineFactory getHyperonEngineFactory() {

		log.info("Engine factory begin creation");
		HyperonEngineFactory hyperonEngineFactory = new HyperonEngineFactory();
		hyperonEngineFactory.setDataSource(getDataSource());

		if (hyperonProperties.getRuntime().isDevMode()) {
			log.info("Hyperon developer mode on");
			hyperonEngineFactory.setDeveloperMode(true);
			hyperonEngineFactory.setUsername(hyperonProperties.getRuntime().getDevUser());
		}
		return hyperonEngineFactory;
	}

	@Bean
	public HyperonEngine getHyperonEngine() {

		return getHyperonEngineFactory().create();
	}

	@Bean
	public BundleService bundleService(HyperonPersistenceFactory hyperonPersistenceFactory) {

		BundleService bundleService = hyperonPersistenceFactory.create();
		HyperonPersistence.setDefaultBundleDef(bundleService.getDefinition(hyperonProperties.getRuntime().getProfile()));
		return bundleService;
	}

	@Bean(destroyMethod = "destroy")
	public HyperonPersistenceFactory hyperonPersistenceFactory() {

		HyperonProperties.Runtime runtime = hyperonProperties.getRuntime();
		HyperonProperties.Persistence persistence = hyperonProperties.getPersistence();

		HyperonPersistenceFactory factory = new HyperonPersistenceFactory();
		factory.setDataSource(primaryDataSource);
		factory.setHyperonDataSource(getDataSource());
		factory.setAutoStartWatchers(true);
		factory.setDefaultProfile(runtime.getProfile());
		factory.setHiloSequenceName(persistence.getSequenceName());
		factory.setHiloAllocationSize(persistence.getAllocationSize());
		factory.setBundleTable(persistence.getBundleTable());
		factory.setBundleColumn(persistence.getBundleColumn());
		factory.setOwnerColumn(persistence.getOwnerColumn());
		factory.setOwnerPropertyColumn(persistence.getOwnerPropertyColumn());
		return factory;
	}
}
