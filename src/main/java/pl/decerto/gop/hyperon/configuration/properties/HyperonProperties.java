package pl.decerto.gop.hyperon.configuration.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "hyperon")
public class HyperonProperties {

	private String username;
	private String password;
	private String url;
	private String dialect;
	private Runtime runtime;
	private Persistence persistence;

	@Data
	public static class Runtime {

		private String profile;
		private String productLine;
		private int initialSize;
		private int maxActive;
		private boolean devMode;
		private String devUser;
	}

	@Data
	public static class Persistence {

		private String sequenceName;
		private int allocationSize;
		private String bundleTable;
		private String bundleColumn;
		private String ownerColumn;
		private String ownerPropertyColumn;
	}
}
