package pl.decerto.gop.hyperon.configuration;

import pl.decerto.gop.hyperon.doa.DictionaryDoa;

public class DataObjectAccessRepository {

	private final DataObjectAccessFactory dataObjectAccessFactory;

	DataObjectAccessRepository(DataObjectAccessFactory dataObjectAccessFactory) {

		this.dataObjectAccessFactory = dataObjectAccessFactory;
	}

	public DictionaryDoa getDictionaryDoa() {

		return this.dataObjectAccessFactory.getDictionaryDoa();
	}

}
