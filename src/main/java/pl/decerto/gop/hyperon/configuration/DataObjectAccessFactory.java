package pl.decerto.gop.hyperon.configuration;

import org.springframework.context.annotation.Configuration;
import pl.decerto.gop.hyperon.configuration.properties.HyperonProperties;
import pl.decerto.gop.hyperon.doa.DictionaryDoa;
import pl.decerto.gop.hyperon.doa.schema.DomainPath;
import pl.decerto.hyperon.runtime.core.HyperonEngine;

@Configuration
public class DataObjectAccessFactory {

	private final HyperonEngine engine;
	private final HyperonProperties properties;

	DataObjectAccessFactory(HyperonEngine engine, HyperonProperties properties) {

		this.engine = engine;
		this.properties = properties;
	}

	public DataObjectAccessRepository build() {

		return new DataObjectAccessRepository(this);
	}

	DictionaryDoa getDictionaryDoa() {

		String profile = properties.getRuntime().getProfile();
		return new DictionaryDoa(engine.getDomain(profile, DomainPath.DICTIONARY.getPath()));
	}

}
