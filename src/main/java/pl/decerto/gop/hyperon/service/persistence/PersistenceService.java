package pl.decerto.gop.hyperon.service.persistence;

import pl.decerto.gop.hyperon.persistence.entities.BundleRoot;
import pl.decerto.gop.model.bundle.BundleId;
import pl.decerto.hyperon.persistence.sync.diff.BundleDiff;

public interface PersistenceService {

	BundleRoot create();

	BundleRoot load(BundleId bundleId);

	BundleDiff persist(BundleRoot bundleRoot);
}
