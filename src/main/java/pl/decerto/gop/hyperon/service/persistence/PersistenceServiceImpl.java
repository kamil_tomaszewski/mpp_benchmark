package pl.decerto.gop.hyperon.service.persistence;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.decerto.gop.hyperon.persistence.entities.BundleRoot;
import pl.decerto.gop.hyperon.persistence.service.HyperonPersistenceService;
import pl.decerto.gop.model.bundle.BundleId;
import pl.decerto.hyperon.persistence.sync.diff.BundleDiff;

@Service
@AllArgsConstructor
public final class PersistenceServiceImpl implements PersistenceService {

	private final HyperonPersistenceService hyperonPersistenceService;

	public BundleRoot create() {

		return hyperonPersistenceService.create();
	}

	public BundleRoot load(BundleId bundleId) {

		return hyperonPersistenceService.load(bundleId.getValue());
	}

	public BundleDiff persist(BundleRoot bundleRoot) {

		BundleDiff bundleDiff = hyperonPersistenceService.diff(bundleRoot);
		hyperonPersistenceService.persist(bundleRoot);
		return bundleDiff;
	}
}
