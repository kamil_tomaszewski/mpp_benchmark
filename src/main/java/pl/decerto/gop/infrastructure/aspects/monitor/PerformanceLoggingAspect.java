package pl.decerto.gop.infrastructure.aspects.monitor;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.Instant;

@Slf4j
@Aspect
@Component
public class PerformanceLoggingAspect {

	@Around("@annotation(performanceLogging)")
	public Object authorizationCheck(ProceedingJoinPoint joinPoint, PerformanceLogging performanceLogging) throws Throwable {

		Instant start = Instant.now();
		Object proceedObject = joinPoint.proceed();
		Instant end = Instant.now();
		log.trace("{} in {}ms", joinPoint.getSignature().getName(), Duration.between(start, end).toMillis());
		return proceedObject;
	}
}
