package pl.decerto.gop.infrastructure.configuration;

import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.unit.DataSize;

import javax.servlet.MultipartConfigElement;

@Configuration
public class MultipartConfiguration {

	private static final long MAX_FILE_SIZE = 20L * 1024L * 1024L;

	@Bean
	public MultipartConfigElement multipartConfigElement() {

		MultipartConfigFactory factory = new MultipartConfigFactory();
		factory.setMaxFileSize(DataSize.ofBytes(MAX_FILE_SIZE));
		factory.setMaxRequestSize(DataSize.ofBytes(MAX_FILE_SIZE));
		return factory.createMultipartConfig();
	}
}
