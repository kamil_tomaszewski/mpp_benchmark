package pl.decerto.gop.infrastructure.exception.application.abstracts;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public abstract class AbstractApplicationException extends RuntimeException {

	private final List<ApplicationError> errors = new ArrayList<>();

	public AbstractApplicationException(ApplicationError error) {

		this.errors.add(error);
	}
}
