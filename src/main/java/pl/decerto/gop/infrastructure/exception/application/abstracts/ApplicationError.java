package pl.decerto.gop.infrastructure.exception.application.abstracts;

import lombok.EqualsAndHashCode;
import lombok.Value;
import pl.decerto.gop.model.error.AbstractError;

@Value
@EqualsAndHashCode(callSuper = false)
public class ApplicationError extends AbstractError {

	public ApplicationError(Object rejectedValue, String errorCode) {

		super(rejectedValue, errorCode);
	}
}
