package pl.decerto.gop.infrastructure.exception.application;

import pl.decerto.gop.infrastructure.exception.application.abstracts.AbstractApplicationException;
import pl.decerto.gop.infrastructure.exception.application.abstracts.ApplicationError;
import pl.decerto.gop.model.rider.RiderId;
import pl.decerto.gop.util.error.ErrorCodes;

public class RiderNotFoundException extends AbstractApplicationException {

	public RiderNotFoundException(RiderId riderId) {

		super(new ApplicationError(riderId, ErrorCodes.Rider.ERR_RIDER_NOT_FOUND));
	}
}
