package pl.decerto.gop.infrastructure.exception.application;

import pl.decerto.gop.infrastructure.exception.application.abstracts.AbstractApplicationException;
import pl.decerto.gop.infrastructure.exception.application.abstracts.ApplicationError;

public class GopIllegalStateException extends AbstractApplicationException {

	public GopIllegalStateException(String message) {

		super(new ApplicationError(message, "ERR_GOP_ILLEGAL_STATE"));
	}
}
