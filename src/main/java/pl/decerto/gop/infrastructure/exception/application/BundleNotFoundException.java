package pl.decerto.gop.infrastructure.exception.application;

import pl.decerto.gop.infrastructure.exception.application.abstracts.AbstractApplicationException;
import pl.decerto.gop.infrastructure.exception.application.abstracts.ApplicationError;
import pl.decerto.gop.model.bundle.BundleId;

public class BundleNotFoundException extends AbstractApplicationException {

	public BundleNotFoundException(BundleId bundleId) {

		super(new ApplicationError(bundleId, "ERR_BUNDLE_NOT_FOUND"));
	}
}
