package pl.decerto.gop.infrastructure.util;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.decerto.gop.application.bundle.BundleEntityH;
import pl.decerto.gop.application.bundle.BundleEntityHRepo;
import pl.decerto.gop.application.bundle.DomainBundleService;
import pl.decerto.gop.application.rider.RiderEntityH;
import pl.decerto.gop.application.scope.ScopeEntityH;
import pl.decerto.gop.application.variant.VariantEntityH;
import pl.decerto.gop.hyperon.persistence.entities.BundleRoot;
import pl.decerto.gop.hyperon.persistence.entities.Rider;
import pl.decerto.gop.hyperon.persistence.entities.Scope;
import pl.decerto.gop.hyperon.persistence.entities.Variant;
import pl.decerto.gop.infrastructure.exception.application.GopIllegalStateException;
import pl.decerto.gop.infrastructure.exception.application.RiderNotFoundException;
import pl.decerto.gop.model.rider.RiderId;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Component
@Transactional
@AllArgsConstructor
public class BundleRootUtils {

	private final BundleEntityHRepo bundleRepoH;
	private final DomainBundleService bundleService;

	public Rider findRider(BundleRoot bundleRoot, RiderId riderId) {

		return getRiderStream(bundleRoot)
				.filter(r -> r.getId() == riderId.getValue())
				.findAny()
				.orElseThrow(() -> new RiderNotFoundException(riderId));
	}

	public List<Rider> toRiderList(BundleRoot bundleRoot) {

		return getRiderStream(bundleRoot)
				.collect(Collectors.toList());
	}

	public Stream<Rider> getRiderStream(BundleRoot bundleRoot) {

		return getVariantStream(bundleRoot)
				.flatMap(v -> v.getRiders().stream());
	}

	public Stream<Scope> getScopesStream(BundleRoot bundleRoot) {

		return bundleRoot.getOffer().getGroups().stream()
				.flatMap(g -> g.getScopes().stream())
				.sorted(Comparator.comparing(Scope::getName));
	}

	public List<RiderEntityH> toRiderList(BundleEntityH bundle) {

		return getRiderStream(bundle)
				.collect(Collectors.toList());
	}

	public Stream<RiderEntityH> getRiderStream(BundleEntityH bundle) {

		return getVariantStream(bundle)
				.flatMap(v -> v.getRiders().stream());
	}

	public Stream<VariantEntityH> getVariantStream(BundleEntityH bundle) {

		return getScopesStream(bundle)
				.flatMap(s -> s.getVariants().stream())
				.sorted(Comparator.comparing(VariantEntityH::getName));
	}

	public Stream<Variant> getVariantStream(BundleRoot bundleRoot) {

		return getScopesStream(bundleRoot)
				.flatMap(s -> s.getVariants().stream())
				.sorted(Comparator.comparing(Variant::getName));
	}

	public Stream<ScopeEntityH> getScopesStream(BundleEntityH bundle) {

		return bundle.getOffer().getGroups().stream()
				.flatMap(g -> g.getScopes().stream())
				.sorted(Comparator.comparing(ScopeEntityH::getName));
	}

	public BundleRoot saveLoadAndCheckRiders(BundleRoot bundle, int riderSize, int membersSize) {

		bundleService.persist(bundle);
		return loadAndCheckAllSize(bundle.getId(), riderSize, membersSize);
	}

	public BundleRoot loadAndCheckAllSize(Long id, int riderSize, int membersSize) {

		BundleRoot bundle = bundleService.load(id);
		return checkAllSize(bundle, riderSize, membersSize);
	}

	public BundleRoot checkAllSize(BundleRoot bundle, int riderSize, int membersSize) {

		checkRidersSize(bundle, riderSize);
		return checkMembersSize(bundle, membersSize);
	}

	public BundleRoot checkRidersSize(BundleRoot bundle, int riderSize) {

		if (toRiderList(bundle).size() != riderSize) {
			throw new GopIllegalStateException(String.format("illegal size: curr %s expected %s", toRiderList(bundle).size(), riderSize));
		}
		return bundle;
	}

	public BundleRoot checkMembersSize(BundleRoot bundle, int membersSize) {

		long size = bundle.getOffer().getGroups().stream().mapToLong(g -> g.getInsureds().size()).sum();
		if (size != membersSize) {
			throw new GopIllegalStateException(String.format("illegal size: curr %s expected %s", size, membersSize));
		}
		return bundle;
	}

	public BundleEntityH saveLoadAndCheckRiders(BundleEntityH bundle, int riderSize, int membersSize) {

		bundleRepoH.saveAndFlush(bundle);
		return loadAndCheckAllSizeH(bundle.getId(), riderSize, membersSize);
	}

	public BundleEntityH loadAndCheckAllSizeH(Long id, int riderSize, int membersSize) {

		BundleEntityH bundle = bundleRepoH.findById(id).get();
		return checkAllSize(bundle, riderSize, membersSize);
	}

	public BundleEntityH checkAllSize(BundleEntityH bundle, int riderSize, int membersSize) {

		checkRidersSize(bundle, riderSize);
		return checkMembersSize(bundle, membersSize);
	}

	public BundleEntityH checkRidersSize(BundleEntityH bundle, int riderSize) {

		if (toRiderList(bundle).size() != riderSize) {
			throw new GopIllegalStateException(String.format("illegal size: curr %s expected %s", toRiderList(bundle).size(), riderSize));
		}
		return bundle;
	}

	public BundleEntityH checkMembersSize(BundleEntityH bundle, int membersSize) {

		long size = bundle.getOffer().getGroups().stream().mapToLong(g -> g.getInsureds().size()).sum();
		if (size != membersSize) {
			throw new GopIllegalStateException(String.format("illegal size: curr %s expected %s", size, membersSize));
		}
		return bundle;
	}
}
