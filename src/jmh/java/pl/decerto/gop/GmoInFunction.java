package pl.decerto.gop;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Setup;
import org.smartparam.engine.core.output.ParamValue;
import pl.decerto.gop.hyperon.persistence.entities.BundleRoot;
import pl.decerto.gop.hyperon.persistence.entities.Group;
import pl.decerto.gop.hyperon.persistence.entities.Insured;
import pl.decerto.gop.hyperon.persistence.entities.Rider;
import pl.decerto.gop.hyperon.persistence.entities.Scope;
import pl.decerto.gop.hyperon.persistence.entities.Variant;
import pl.decerto.hyperon.persistence.context.AppCtx;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class GmoInFunction extends Abstract {

	private BundleRoot bundle;

	@Setup(Level.Trial)
	public void setupIteration() {

		bundle = offerService.createBundleWith(4, 4, 4, 1000);
	}

	@Benchmark
	public BigDecimal loopJava() {

		BigDecimal result = BigDecimal.ZERO;

		for (Group group : bundle.getOffer().getGroups()) {
			for (Scope scope : group.getScopes()) {
				for (Variant variant : scope.getVariants()) {
					for (Rider rider : variant.getRiders()) {
						for (Insured insured : group.getInsureds()) {
							result = calc(result, group.getName(), scope.getName(), variant.getName(), rider, insured);
						}
					}
				}
			}
		}

		if (result.compareTo(BigDecimal.valueOf(0)) == 0) {
			throw new RuntimeException(String.format("illegal result: curr %s", result));
		}
		return result;
	}

	@Benchmark
	public BigDecimal loopHyperon() {

		AppCtx ctx = new AppCtx(bundle.unproxy());
		ParamValue param = engine.get("benchmark.loooop", ctx);
		BigDecimal result = param.getBigDecimal();

		if (result.compareTo(BigDecimal.valueOf(0)) == 0) {
			throw new RuntimeException(String.format("illegal result: curr %s", result));
		}
		return result;
	}

	@Benchmark
	public BigDecimal sendBundleToHyperon() {

		AppCtx ctx = new AppCtx(bundle.unproxy());
		BigDecimal result = (BigDecimal) engine.call("benchmark.nothingForBundle", ctx);

		if (result.compareTo(BigDecimal.valueOf(0)) == 0) {
			throw new RuntimeException(String.format("illegal result: curr %s", result));
		}
		return result;
	}

	private BigDecimal calc(BigDecimal result, String groupName, String scopeName, String variantName, Rider rider, Insured insured) {

		BigDecimal salary = insured.getSalary();
		BigDecimal assuredSum = rider.getAssuredSum();
		result = result.add(salary.divide(assuredSum, RoundingMode.HALF_UP));
		if (variantName.length() > 5) {
			result = result.add(BigDecimal.ONE);
		}
		if (scopeName.length() > 5) {
			result = result.add(BigDecimal.ONE);
		}
		if (groupName.length() > 5) {
			result = result.add(BigDecimal.ONE);
		}
		return result;
	}
}
