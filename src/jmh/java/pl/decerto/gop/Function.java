package pl.decerto.gop;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import pl.decerto.gop.hyperon.doa.DictionaryDoa;
import pl.decerto.hyperon.runtime.core.AdhocContext;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@OutputTimeUnit(TimeUnit.NANOSECONDS)
public class Function extends Abstract {

	public static final Random RANDOM = new Random();

	@Benchmark
	public Boolean constHyperonDomain() {

		DictionaryDoa dictionaryDoa = doaFactory.build().getDictionaryDoa();
		return dictionaryDoa.getConst(getRandomString());
	}

	@Benchmark
	public Boolean constHyperon() {

		AdhocContext ctx = new AdhocContext();
		ctx.set("formula", getRandomString());
		return (boolean) (Boolean) engine.call("benchmark.const", ctx);
	}

	@Benchmark
	public Boolean constJava() {

		return getRandomString().equals("ABC");
	}

	private String getRandomString() {

		return Long.toString(RANDOM.nextLong());
	}

}
