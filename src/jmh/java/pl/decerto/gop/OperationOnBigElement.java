package pl.decerto.gop;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Setup;
import pl.decerto.gop.application.bundle.BundleEntityH;
import pl.decerto.gop.hyperon.persistence.entities.BundleRoot;
import pl.decerto.hyperon.persistence.sync.diff.BundleDiff;

public class OperationOnBigElement extends Abstract {

	private BundleRoot bundle;
	private BundleEntityH bundleH;

	@Setup(Level.Iteration)
	public void setupIteration() {

		bundle = offerService.createBundleWith(4, 4, 4, 1000);
		bundleService.persist(bundle);
		bundleH = offerService.createBundleHWith(4, 4, 4, 1000);
		bundleRepoH.saveAndFlush(bundleH);
	}

	@Benchmark
	public BundleDiff addInsuredToBigBundle() {

		return insuredService.addInsuredLoadSave(bundle);
	}

	@Benchmark
	public BundleEntityH addInsuredToBigBundleH() {

		return insuredService.addInsuredHLoadSave(bundleH);
	}

	@Benchmark
	public BundleRoot read() {

		return bundleRootUtils.checkAllSize(bundle, 4 * 4 * 4 * 50, 4000);
	}

	@Benchmark
	public BundleEntityH readH() {

		return bundleRootUtils.checkAllSize(bundleH, 4 * 4 * 4 * 50, 4000);
	}

	@Benchmark
	public BundleRoot loadAndRead() {

		return bundleRootUtils.loadAndCheckAllSize(bundle.getId(), 4 * 4 * 4 * 50, 4000);
	}

	@Benchmark
	public BundleEntityH loadAndReadH() {

		return bundleRootUtils.loadAndCheckAllSizeH(bundle.getId(), 4 * 4 * 4 * 50, 4000);
	}
}
