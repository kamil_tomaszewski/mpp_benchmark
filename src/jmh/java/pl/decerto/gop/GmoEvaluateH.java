package pl.decerto.gop;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Setup;
import pl.decerto.gop.application.bundle.BundleEntityH;
import pl.decerto.gop.application.group.GroupEntityH;
import pl.decerto.gop.application.insured.InsuredEntityH;
import pl.decerto.gop.application.rider.RiderEntityH;
import pl.decerto.gop.application.scope.ScopeEntityH;
import pl.decerto.gop.application.variant.VariantEntityH;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class GmoEvaluateH extends Abstract {

	private BundleEntityH bundleH;

	@Setup(Level.Iteration)
	public void setupIteration() {

		bundleH = offerService.createBundleHWith(4, 4, 4, 1000);
		bundleRepoH.saveAndFlush(bundleH);
	}

	@Benchmark
	public BigDecimal loopClear() {

		return transactionHandler.runInTransaction(() -> {
			bundleH = bundleRepoH.findById(bundleH.getId()).get();

			BigDecimal result = BigDecimal.ZERO;

			for (GroupEntityH group : bundleH.getOffer().getGroups()) {
				for (ScopeEntityH scope : group.getScopes()) {
					for (VariantEntityH variant : scope.getVariants()) {
						for (RiderEntityH rider : variant.getRiders()) {
							for (InsuredEntityH insured : group.getInsureds()) {
								result = calc(result, group.getName(), scope.getName(), variant.getName(), rider, insured);
							}
						}
					}
				}
			}

			if (result.compareTo(BigDecimal.valueOf(0)) == 0) {
				throw new RuntimeException(String.format("illegal result: curr %s", result));
			}
			return result;
		});
	}

	@Benchmark
	public BigDecimal loopOpt() {

		return transactionHandler.runInTransaction(() -> {
			bundleH = bundleRepoH.findById(bundleH.getId()).get();
			BigDecimal result = BigDecimal.ZERO;

			for (GroupEntityH group : bundleH.getOffer().getGroups()) {
				String groupName = group.getName();
				for (ScopeEntityH scope : group.getScopes()) {
					String scopeName = scope.getName();
					for (VariantEntityH variant : scope.getVariants()) {
						String variantName = variant.getName();
						for (RiderEntityH rider : variant.getRiders()) {
							for (InsuredEntityH insured : group.getInsureds()) {
								result = calc(result, groupName, scopeName, variantName, rider, insured);
							}
						}
					}
				}
			}

			if (result.compareTo(BigDecimal.valueOf(0)) == 0) {
				throw new RuntimeException(String.format("illegal result: curr %s", result));
			}
			return result;
		});
	}

	private BigDecimal calc(BigDecimal result, String groupName, String scopeName, String variantName, RiderEntityH rider, InsuredEntityH insured) {

		BigDecimal salary = insured.getSalary();
		BigDecimal assuredSum = rider.getAssuredSum();
		if (!assuredSum.equals(BigDecimal.ZERO)) {
			result = result.add(salary.divide(assuredSum, RoundingMode.HALF_UP));
		}
		if (variantName.length() > 5) {
			result = result.add(BigDecimal.ONE);
		}
		if (scopeName.length() > 5) {
			result = result.add(BigDecimal.ONE);
		}
		if (groupName.length() > 5) {
			result = result.add(BigDecimal.ONE);
		}
		return result;
	}

}
