package pl.decerto.gop;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Setup;
import pl.decerto.gop.application.bundle.BundleEntityH;
import pl.decerto.gop.application.insured.InsuredEntityH;
import pl.decerto.gop.application.variant.VariantService;
import pl.decerto.gop.hyperon.persistence.entities.BundleRoot;
import pl.decerto.gop.hyperon.persistence.entities.Insured;
import pl.decerto.hyperon.persistence.sync.diff.BundleDiff;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Evaluate extends Abstract {

	private BundleEntityH bundleH;
	private BundleRoot bundle;

	@Setup(Level.Iteration)
	public void setupIteration() {

		bundleH = offerService.createBundleHWith(4, 4, 4, 1000);
		bundleRepoH.saveAndFlush(bundleH);
		bundle = offerService.createBundleWith(4, 4, 4, 1000);
		bundleService.persist(bundle);
	}

	@Benchmark
	public BundleRoot evaluateGroupsScopesVariantsX4SimulateCalculation() {

		return transactionHandler.runInTransaction(() -> {
			bundle = bundleService.load(bundle.getId());

			bundle.getOffer().getGroups().forEach(group -> group.getScopes().forEach(scope -> scope.getVariants().stream()
							.sorted(VariantService.VARIANT_PRIORITY_COMPARATOR).forEach(variant -> {
								variant.getRiders()
										.forEach(rider -> {
											BigDecimal sum = BigDecimal.ZERO;
											for (Insured insured : group.getInsureds()) {
												sum = calculate(sum, rider.getCode(), insured.getBirthYear(), rider.getAssuredSum(), insured.getSalary());
											}
											rider.setUid(sum.toString());
										});
							})
					)
			);
			return bundle;
		});
	}

	@Benchmark
	public BundleDiff evaluateGroupsScopesVariantsX4SimulateCalculationSave() {

		return transactionHandler.runInTransaction(() -> {
			bundle = bundleService.load(bundle.getId());

			bundle.getOffer().getGroups().forEach(group -> group.getScopes().forEach(scope -> scope.getVariants().stream()
							.sorted(VariantService.VARIANT_PRIORITY_COMPARATOR).forEach(variant -> {
								variant.getRiders()
										.forEach(rider -> {
											BigDecimal sum = BigDecimal.ZERO;
											for (Insured insured : group.getInsureds()) {
												sum = calculate(sum, rider.getCode(), insured.getBirthYear(), rider.getAssuredSum(), insured.getSalary());
											}
											rider.setUid(sum.toString());
										});
							})
					)
			);
			return bundleService.persist(bundle);
		});
	}

	@Benchmark
	public BundleEntityH evaluateGroupsScopesVariantsX4SimulateCalculationH() {

		return transactionHandler.runInTransaction(() -> {
			bundleH = bundleRepoH.findById(bundleH.getId()).get();

			bundleH.getOffer().getGroups().forEach(group -> group.getScopes().forEach(scope -> scope.getVariants().stream()
							.sorted(VariantService.VARIANT_PRIORITY_COMPARATOR_H).forEach(variant -> {
								variant.getRiders()
										.forEach(rider -> {
											BigDecimal sum = BigDecimal.ZERO;
											for (InsuredEntityH insured : group.getInsureds()) {
												sum = calculate(sum, rider.getCode(), insured.getBirthYear(), rider.getAssuredSum(), insured.getSalary());
											}
											rider.setUuid(sum.toString());
										});
							})
					)
			);
			return bundleH;
		});
	}
	@Benchmark
	public BundleEntityH evaluateGroupsScopesVariantsX4SimulateCalculationHSave() {

		return transactionHandler.runInTransaction(() -> {
			bundleH = bundleRepoH.findById(bundleH.getId()).get();

			bundleH.getOffer().getGroups().forEach(group -> group.getScopes().forEach(scope -> scope.getVariants().stream()
							.sorted(VariantService.VARIANT_PRIORITY_COMPARATOR_H).forEach(variant -> {
								variant.getRiders()
										.forEach(rider -> {
											BigDecimal sum = BigDecimal.ZERO;
											for (InsuredEntityH insured : group.getInsureds()) {
												sum = calculate(sum, rider.getCode(), insured.getBirthYear(), rider.getAssuredSum(), insured.getSalary());
											}
											rider.setUuid(sum.toString());
										});
							})
					)
			);
			return bundleRepoH.saveAndFlush(bundleH);
		});
	}

	private BigDecimal calculate(BigDecimal sum, String code, String birthYear, BigDecimal assuredSum, BigDecimal salary) {

		BigDecimal add = BigDecimal.ZERO;
		if (code.equals(birthYear)) {
			add = BigDecimal.ONE;
		}
		BigDecimal result = assuredSum.add(salary).divide(BigDecimal.valueOf(2), RoundingMode.HALF_UP);
		sum = result.add(sum).divide(BigDecimal.valueOf(2), RoundingMode.HALF_UP).add(add);
		return sum;
	}
}
