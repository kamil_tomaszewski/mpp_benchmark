package pl.decerto.gop;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Setup;
import pl.decerto.gop.application.bundle.BundleEntityH;
import pl.decerto.gop.hyperon.persistence.entities.BundleRoot;
import pl.decerto.hyperon.persistence.sync.diff.BundleDiff;

public class OperationOnSmallElement extends Abstract {

	private BundleRoot bundle;
	private BundleEntityH bundleH;

	@Setup(Level.Iteration)
	public void setupIteration() {

		bundle = offerService.createBundleWith(1, 2, 2, 100);
		bundleService.persist(bundle);
		bundleH = offerService.createBundleHWith(1, 2, 2, 100);
		bundleRepoH.saveAndFlush(bundleH);
	}

	@Benchmark
	public BundleDiff addInsured() {

		return insuredService.addInsuredLoadSave(bundle);
	}

	@Benchmark
	public BundleEntityH addInsuredH() {

		return insuredService.addInsuredHLoadSave(bundleH);
	}

	@Benchmark
	public BundleRoot read() {

		return bundleRootUtils.checkAllSize(bundle, 200, 100);
	}

	@Benchmark
	public BundleEntityH readH() {

		return bundleRootUtils.checkAllSize(bundleH, 200, 100);
	}

	@Benchmark
	public BundleRoot loadAndRead() {

		bundle = bundleService.load(bundle.getId());
		return bundleRootUtils.loadAndCheckAllSize(bundle.getId(), 200, 100);
	}

	@Benchmark
	public BundleEntityH loadAndReadH() {

		bundleH = bundleRepoH.findById(bundleH.getId()).get();
		return bundleRootUtils.loadAndCheckAllSizeH(bundleH.getId(), 200, 100);
	}

}
