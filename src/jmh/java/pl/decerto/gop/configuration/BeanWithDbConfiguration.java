package pl.decerto.gop.configuration;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import pl.decerto.gop.hyperon.service.persistence.PersistenceService;
import pl.decerto.gop.hyperon.service.persistence.PersistenceServiceImpl;

@TestConfiguration
public class BeanWithDbConfiguration {

	@Bean
	@Primary
	PersistenceService persistenceService(PersistenceServiceImpl persistenceService) {

		return persistenceService;
	}
}

