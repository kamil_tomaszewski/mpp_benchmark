package pl.decerto.gop.configuration;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.test.context.ActiveProfilesResolver;
import pl.decerto.gop.util.StringUtils;

import java.util.Objects;

/**
 * @author Krzysztof Wilczak <krzysztof.wilczak@nn.pl>
 * @since 2018-05-24
 * This class is necessary because of problem:
 * https://jira.spring.io/browse/SPR-8982
 * founded solution can be found in:
 * https://stackoverflow.com/questions/20551681/spring-integration-tests-with-profile/33044283#33044283
 * <p>
 * Problem is that test configuration does not use spring.active.profile property. All properties are always read from application.yaml
 * (not from application-{profileName}.yaml)
 * Solution is creating custom active profiles resolver for tests
 */
@TestConfiguration
public class SystemPropertyActiveProfileResolver implements ActiveProfilesResolver {

	private static final String DEFAULT_PROFILE = "test";

	@Override
	public String[] resolve(Class<?> testClass) {

		if (System.getProperties().containsKey("spring.profiles.active")) {
			//read properties from application-{profileName}.yaml file
			final String profiles = System.getProperty("spring.profiles.active");
			if (Objects.nonNull(profiles)) {
				return profiles.split("\\s*,\\s*");
			} else {
				return new String[]{StringUtils.EMPTY};
			}
		} else {
			//read properties from application.yaml file
			return new String[]{DEFAULT_PROFILE};
		}
	}
}
