package pl.decerto.gop;

import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import pl.decerto.gop.application.bundle.BundleEntityH;
import pl.decerto.gop.application.bundle.BundleEntityHRepo;
import pl.decerto.gop.application.bundle.DomainBundleService;
import pl.decerto.gop.application.group.GroupService;
import pl.decerto.gop.application.insured.InsuredService;
import pl.decerto.gop.application.offer.OfferService;
import pl.decerto.gop.hyperon.configuration.DataObjectAccessFactory;
import pl.decerto.gop.hyperon.persistence.entities.BundleRoot;
import pl.decerto.gop.infrastructure.util.BundleRootUtils;
import pl.decerto.gop.util.TransactionHandler;
import pl.decerto.hyperon.runtime.core.HyperonEngine;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.concurrent.TimeUnit;

//@Measurement(iterations = 5)
//@Warmup(iterations = 10)
//@Fork(3)
//@Threads(6)
//@Measurement(iterations = 2)
//@Fork(value = 0, warmups = 0)
//@Warmup(iterations = 0)
@Fork(value = 1) // test powtarzamy tylko dla jednego procesu
//@Warmup(iterations = 2, time = 20)
//@Measurement(iterations = 2, time = 20)
//@Warmup(iterations = 1, batchSize = 20) // jedna iteracja rozgrzewkowa uruchomiona 100 razy
//@Measurement(iterations = 2, batchSize = 20) // dwie iteracje pomiarowe, uruchomione 20 razy
@Warmup(iterations = 2, time = 10)
@Measurement(iterations = 2, time = 10)
//@Warmup(iterations = 1, time = 2) // jedna iteracja rozgrzewkowa trwająca 2 sekundy
//@Measurement(iterations = 2, time = 2) // dwie iteracje pomiarowe, każda trwające 2 sekundy
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(org.openjdk.jmh.annotations.Scope.Thread)
abstract class Abstract {

	protected static ConfigurableApplicationContext context;
	protected OfferService offerService;
	protected GroupService groupService;
	protected InsuredService insuredService;
	protected BundleRootUtils bundleRootUtils;
	protected BundleEntityHRepo bundleRepoH;
	protected DomainBundleService bundleService;
	protected DataObjectAccessFactory doaFactory;
	protected HyperonEngine engine;
	protected TransactionHandler transactionHandler;

	public static void main(String[] args) throws Exception {

		URLClassLoader classLoader = (URLClassLoader) Abstract.class.getClassLoader();
		StringBuilder classpath = new StringBuilder();
		for (URL url : classLoader.getURLs())
			classpath.append(url.getPath()).append(File.pathSeparator);
		classpath.append("/home/kamil/projects/mpp_benchmark/src/jmh/resources").append(File.pathSeparator);
		System.out.print(classpath.toString());
		System.setProperty("java.class.path", classpath.toString());

		Options opt = new OptionsBuilder()
				.include(Abstract.class.getName() + ".*")
				.timeUnit(TimeUnit.MILLISECONDS)
				.threads(1)
				.shouldFailOnError(true)
				.shouldDoGC(true)
				.build();

		Runner runner = new Runner(opt);
		runner.run();
	}

	BundleRoot createBundle() {

		return offerService.create();
	}

	BundleEntityH createBundleH() {

		return offerService.createEntity();
	}

	@Setup(Level.Trial)
	public synchronized void initialize() {

		try {
			if (context == null) {
				context = SpringApplication.run(GroupOnlinePlatformApplication.class);
				offerService = context.getBean(OfferService.class);
				groupService = context.getBean(GroupService.class);
				bundleRootUtils = context.getBean(BundleRootUtils.class);
				insuredService = context.getBean(InsuredService.class);
				bundleRepoH = context.getBean(BundleEntityHRepo.class);
				bundleService = context.getBean(DomainBundleService.class);
				doaFactory = context.getBean(DataObjectAccessFactory.class);
				engine = context.getBean(HyperonEngine.class);
				transactionHandler = context.getBean(TransactionHandler.class);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@TearDown(Level.Trial)
	public void close() {

		context.close();
	}
}
