package pl.decerto.gop;

import org.openjdk.jmh.annotations.Benchmark;
import pl.decerto.gop.application.bundle.BundleEntityH;
import pl.decerto.gop.hyperon.persistence.entities.BundleRoot;

public class Basic extends Abstract {

	@Benchmark
	public BundleRoot createBundleWithOneGroupSaveLoadAndCheckRiders() {

		return transactionHandler.runInTransaction(() -> {
			BundleRoot bundle = createBundle();
			return bundleRootUtils.saveLoadAndCheckRiders(bundle, 50, 0);
		});
	}

	@Benchmark
	public BundleEntityH createBundleWithOneGroupSaveLoadAndCheckRidersH() {

		return transactionHandler.runInTransaction(() -> {
			BundleEntityH bundleH = createBundleH();
			return bundleRootUtils.saveLoadAndCheckRiders(bundleH, 50, 0);
		});
	}

	@Benchmark
	public BundleRoot createBundleWithTwoGroupSaveLoadAndCheckRiders() {

		return transactionHandler.runInTransaction(() -> {
			BundleRoot bundle = createBundle();
			groupService.add(bundle.getOffer(), "second");
			return bundleRootUtils.saveLoadAndCheckRiders(bundle, 2 * 50, 0);
		});
	}

	@Benchmark
	public BundleEntityH createBundleWithTwoGroupSaveLoadAndCheckRidersH() {

		return transactionHandler.runInTransaction(() -> {
			BundleEntityH bundle = createBundleH();
			groupService.addEntity(bundle.getOffer(), "second");
			return bundleRootUtils.saveLoadAndCheckRiders(bundle, 2 * 50, 0);
		});
	}

	@Benchmark
	public BundleRoot createBundleWith10GroupSaveLoadAndCheckRiders() {

		return transactionHandler.runInTransaction(() -> {
			BundleRoot bundle = createBundle();
			for (int i = 0; i < 9; i++) {
				groupService.add(bundle.getOffer(), "groupName" + i);
			}
			return bundleRootUtils.saveLoadAndCheckRiders(bundle, 10 * 50, 0);
		});
	}

	@Benchmark

	public BundleEntityH createBundleWith10GroupSaveLoadAndCheckRidersH() {

		return transactionHandler.runInTransaction(() -> {
			BundleEntityH bundle = createBundleH();
			for (int i = 0; i < 9; i++) {
				groupService.addEntity(bundle.getOffer(), "groupName" + i);
			}
			return bundleRootUtils.saveLoadAndCheckRiders(bundle, 10 * 50, 0);
		});
	}

	@Benchmark
	public BundleRoot createGroupsScopesVariantsX4And100MembersSaveLoadAndCheckRiders() {

		return transactionHandler.runInTransaction(() -> {
			BundleRoot bundle = offerService.createBundleWith(4, 4, 4, 100);
			return bundleRootUtils.saveLoadAndCheckRiders(bundle, 4 * 4 * 4 * 50, 400);
		});
	}

	@Benchmark
	public BundleEntityH createGroupsScopesVariantsX4And100MembersSaveLoadAndCheckRidersH() {

		return transactionHandler.runInTransaction(() -> {
			BundleEntityH bundle = offerService.createBundleHWith(4, 4, 4, 100);
			return bundleRootUtils.saveLoadAndCheckRiders(bundle, 4 * 4 * 4 * 50, 400);
		});
	}
}
