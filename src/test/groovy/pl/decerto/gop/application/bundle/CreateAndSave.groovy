package pl.decerto.gop.application.bundle


import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.transaction.annotation.Transactional
import pl.decerto.gop.application.offer.OfferService
import pl.decerto.gop.hyperon.persistence.entities.BundleRoot
import pl.decerto.gop.infrastructure.util.BundleRootUtils
import spock.lang.Specification

@Slf4j
@SpringBootTest
class CreateAndSave extends Specification {

	@Autowired
	OfferService offerService

	@Autowired
	BundleRootUtils bundleRootUtils

	@Transactional
	def "bundle"() {
		when:
			BundleRoot bundle = offerService.createBundleWith(4, 4, 4, 1000)
			bundle = bundleRootUtils.saveLoadAndCheckRiders(bundle, 4 * 4 * 4 * 50, 4000)

			log.info(bundle.offer.uid)

		then:
			bundle.id > 0
	}

	@Transactional
	def "bundleH"() {
		when:
			BundleEntityH bundle = offerService.createBundleHWith(4, 4, 4, 1000)
			bundle = bundleRootUtils.saveLoadAndCheckRiders(bundle, 4 * 4 * 4 * 50, 4000)
			log.info(bundle.offer.uuid)

		then:
			bundle.id > 0
	}
}
