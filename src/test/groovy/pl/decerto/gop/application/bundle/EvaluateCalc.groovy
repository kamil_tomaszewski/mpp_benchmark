package pl.decerto.gop.application.bundle

import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.transaction.annotation.Transactional
import pl.decerto.gop.application.group.GroupEntityH
import pl.decerto.gop.application.insured.InsuredEntityH
import pl.decerto.gop.application.offer.OfferService
import pl.decerto.gop.application.rider.RiderEntityH
import pl.decerto.gop.application.scope.ScopeEntityH
import pl.decerto.gop.application.variant.VariantEntityH
import pl.decerto.gop.hyperon.persistence.entities.*
import pl.decerto.gop.infrastructure.aspects.monitor.PerformanceLogging
import pl.decerto.gop.infrastructure.util.BundleRootUtils
import spock.lang.Specification

import java.math.RoundingMode
import java.time.Duration
import java.time.Instant

@Slf4j
@SpringBootTest
class EvaluateCalc extends Specification {

	@Autowired
	OfferService offerService

	@Autowired
	BundleRootUtils bundleRootUtils

	@Autowired
	BundleRootUtils bundleRepoH

	Long bundleId

	Long bundleHId

	void setup() {
		BundleRoot bundle = offerService.createBundleWith(4, 4, 4, 1000)
		bundleId = bundleRootUtils.saveLoadAndCheckRiders(bundle, 4 * 4 * 4 * 50, 4000).getId()

		BundleEntityH bundleH = offerService.createBundleHWith(4, 4, 4, 1000)
		bundleHId = bundleRootUtils.saveLoadAndCheckRiders(bundleH, 4 * 4 * 4 * 50, 4000).getId()
	}

	@PerformanceLogging
	@Transactional
	def "bundle"() {
		given:
			Instant start = Instant.now()
			BundleRoot bundle = bundleRootUtils.loadAndCheckAllSize(bundleId, 4 * 4 * 4 * 50, 4000)

		when:
			BigDecimal result = BigDecimal.ZERO

			for (Group group : bundle.getOffer().getGroups()) {
				for (Scope scope : group.getScopes()) {
					for (Variant variant : scope.getVariants()) {
						for (Rider rider : variant.getRiders()) {
							for (Insured insured : group.getInsureds()) {
								result = calc(result, group.getName(), scope.getName(), variant.getName(), rider.assuredSum, insured.salary)
							}
						}
					}
				}
			}

			log.info(bundle.offer.uid)
		then:
			bundle.id > 0
			log.info("{} in {}ms", "bundle", Duration.between(start, Instant.now()).toMillis())

	}

	@PerformanceLogging
	@Transactional
	def "bundleH"() {
		given:
			Instant start = Instant.now()
			BundleEntityH bundleH = bundleRootUtils.loadAndCheckAllSizeH(bundleHId, 4 * 4 * 4 * 50, 4000)

		when:
			BigDecimal result = BigDecimal.ZERO

			for (GroupEntityH group : bundleH.getOffer().getGroups()) {
				for (ScopeEntityH scope : group.getScopes()) {
					for (VariantEntityH variant : scope.getVariants()) {
						for (RiderEntityH rider : variant.getRiders()) {
							for (InsuredEntityH insured : group.getInsureds()) {
								result = calc(result, group.getName(), scope.getName(), variant.getName(), rider.assuredSum, insured.salary)
							}
						}
					}
				}
			}

		then:
			bundleH.id > 0
			log.info("{} in {}ms", "bundleH", Duration.between(start, Instant.now()).toMillis())
	}

	private BigDecimal calc(BigDecimal result, String groupName, String scopeName, String variantName, BigDecimal riderAssuredSum, BigDecimal insuredSalary) {

		result = result.add(insuredSalary.divide(riderAssuredSum, RoundingMode.HALF_UP))
		if (variantName.length() > 5) {
			result = result.add(BigDecimal.ONE)
		}
		if (scopeName.length() > 5) {
			result = result.add(BigDecimal.ONE)
		}
		if (groupName.length() > 5) {
			result = result.add(BigDecimal.ONE)
		}
		return result
	}

}
