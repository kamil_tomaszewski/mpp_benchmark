package pl.decerto.gop.application.bundle


import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.transaction.annotation.Transactional
import pl.decerto.gop.application.offer.OfferService
import pl.decerto.gop.hyperon.persistence.entities.BundleRoot
import pl.decerto.gop.infrastructure.aspects.monitor.PerformanceLogging
import pl.decerto.gop.infrastructure.util.BundleRootUtils
import spock.lang.Specification

import java.time.Duration
import java.time.Instant

@Slf4j
@SpringBootTest
class Load extends Specification {

	@Autowired
	OfferService offerService

	@Autowired
	BundleRootUtils bundleRootUtils

	@Autowired
	BundleRootUtils bundleRepoH

	Long bundleId

	Long bundleHId

	void setup() {
		BundleRoot bundle = offerService.createBundleWith(4, 4, 4, 1000)
		bundleId = bundleRootUtils.saveLoadAndCheckRiders(bundle, 4 * 4 * 4 * 50, 4000).getId()

		BundleEntityH bundleH = offerService.createBundleHWith(4, 4, 4, 1000)
		bundleHId = bundleRootUtils.saveLoadAndCheckRiders(bundleH, 4 * 4 * 4 * 50, 4000).getId()
	}

	@PerformanceLogging
	@Transactional
	def "bundle"() {
		given:
			Instant start = Instant.now()
		when:
			BundleRoot bundle = bundleRootUtils.loadAndCheckAllSize(bundleId, 4 * 4 * 4 * 50, 4000)
			log.info(bundle.offer.uid)
		then:
			bundle.id > 0
			log.info("{} in {}ms", "bundle", Duration.between(start, Instant.now()).toMillis())

	}

	@PerformanceLogging
	@Transactional
	def "bundleH"() {
		given:
			Instant start = Instant.now()

		when:
			BundleEntityH bundleH = bundleRootUtils.loadAndCheckAllSizeH(bundleHId, 4 * 4 * 4 * 50, 4000)
			log.info(bundleH.offer.uuid)

		then:
			bundleH.id > 0
			log.info("{} in {}ms", "bundleH", Duration.between(start, Instant.now()).toMillis())
	}
}
